declare module '*.module.css' {
  const classes: { readonly [key: string]: string };
  export default classes;
}

declare module '*.module.less' {
  const classes: { readonly [key: string]: string };
  export default classes;
}
declare module 'lodash';
declare module 'enzyme-adapter-react-16';
declare module 'aria-query"'